﻿using System;
using System.Collections;
using UnityEngine;

public class IntroManager : MonoBehaviour {
    public float introLengthInSeconds;
    // TODO Maybe flash a light as well
    GameObject[] lightningBolts;

    private IEnumerator DisplayLightning() {
        while(introLengthInSeconds > 0) {
            var selectedBolt = UnityEngine.Random.Range(0, lightningBolts.Length);
            lightningBolts[selectedBolt].SetActive(true);
            var sleepTime = UnityEngine.Random.Range(.1f, .5f);
            introLengthInSeconds -= sleepTime;
            yield return new WaitForSeconds(sleepTime);
            lightningBolts[selectedBolt].SetActive(false);
            sleepTime = UnityEngine.Random.Range(.01f, .2f);
            introLengthInSeconds -= sleepTime;
            yield return new WaitForSeconds(sleepTime);
        }
        var campFire = GameObject.Find("CampFire").GetComponent<CampfireFueled>();
        campFire.StartTheFire();
        Destroy(gameObject);
    }

    // Use this for initialization
    void Start() {
        lightningBolts = new GameObject[transform.childCount];
        for(int i = 0; i < lightningBolts.Length; i++) {
            lightningBolts[i] = transform.GetChild(i).gameObject;
            lightningBolts[i].SetActive(false);
        }
        StartCoroutine(DisplayLightning());
    }
}