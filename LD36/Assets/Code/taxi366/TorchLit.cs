﻿using System.Collections;
using UnityEngine;

public class TorchLit : MonoBehaviour {
    /*
    Torch must have a light component
    */

    public Light myLight;

    public float timeToStayLit;

    //relight the torch
    public void Relight() {
        timeToStayLit = Random.Range(10f, 50f);
        myLight.intensity = timeToStayLit / 100;
    }

    void DecreaceLighting() {
        myLight.intensity -= (timeToStayLit / 100) * Time.deltaTime;
    }

    void Start() {
        Relight();
    }

    void Update() {
        DecreaceLighting();
    }
}