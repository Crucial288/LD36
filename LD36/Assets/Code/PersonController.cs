﻿using System;
using System.Collections;
using UnityEngine;

public class PersonController : EntityController {
    public NavMeshAgent agent;
    Coroutine routine;
    float speed = .1f;

    public override void Awake() {
        base.Awake();
        agent = GetComponent<NavMeshAgent>();
        MouseController.people.Add(this);
    }

    public void OnDisable() {
        MouseController.people.Remove(this);
    }

    public void OnTriggerStay(Collider other) {
        var tree = other.GetComponent<Tree>();
        if(tree != null) {
            tree.Chop();
        }
    }

    internal void InteractWith(EnvironmentController selectedEnvironment, int buttonId) {
        if(routine != null) {
            StopCoroutine(routine);
        }
        //routine = StartCoroutine(GoTo(selectedEnvironment));
        agent.SetDestination(selectedEnvironment.transform.position);
    }

    //private IEnumerator GoTo(EnvironmentController selectedEnvironment) {
    //    var deltaPosition = transform.position - selectedEnvironment.transform.position;
    //    while(deltaPosition.magnitude > 3f) {
    //        transform.position = Vector3.MoveTowards(transform.position, selectedEnvironment.transform.position, speed);
    //        yield return 0;
    //        deltaPosition = transform.position - selectedEnvironment.transform.position;
    //    }
    //}
}